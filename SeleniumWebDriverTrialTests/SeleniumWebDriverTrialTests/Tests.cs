using System.IO;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace SeleniumWebDriverTrialTests
{
    public class Tests
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            //Create driver
            driver = new ChromeDriver();
        }
        [TearDown]
        public void TearDown()
        {
            //Destroy driver
            driver.Quit();
        }
        [Test]
        public void HTMLSwitchFramesTest()
        {
            //Open link
            driver.Navigate().GoToUrl("https://html.com/tags/iframe/");
            //Get iFrame of video
            IWebElement videoFrame = driver.FindElement(By.XPath("//*[@id='post-164']/div/div[4]/iframe"));
            //Scroll tab to frame
            new Actions(driver)
                .MoveToElement(videoFrame)
                .Build()
                .Perform();
            Thread.Sleep(5000);
            //Change drivers focus frame
            driver.SwitchTo().Frame(videoFrame);
            //Get video <a> element
            IWebElement video = driver.FindElement(By.XPath("//*[contains(@id,'player_uid_')]/div[3]/div[2]/div/a"));
            //Check if video title is "Massive volcanoes & Flamingo colony - Wild South America - BBC"
            Assert.AreEqual("Massive volcanoes & Flamingo colony - Wild South America - BBC", video.Text);
            //Get youtube link to the video
            string videoLink = video.GetAttribute("href");
            //Open youtube link
            driver.Navigate().GoToUrl(videoLink);
            Thread.Sleep(5000);
            //Get element with id "country-code"
            IWebElement webElement = driver.FindElement(By.Id("country-code"));
            //Check if this video element text is "BY"
            Assert.AreEqual("BY", webElement.Text);
        }
        [Test]
        public void GithubTabActionsTest()
        {
            //Open link
            driver.Navigate().GoToUrl("https://github.com/");
            //Maximize window size
            driver.Manage().Window.Maximize();
            //Mouse over "Why GitHub?"
            new Actions(driver)
                .MoveToElement(driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/nav/ul/li[1]/details/summary")))
                .Build()
                .Perform();
            //Click on "Actions" 
            driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/nav/ul/li[1]/details/div/ul[1]/li[4]/a")).Click();//
            //Input "Selenium" and Enter pressing
            driver.FindElement(By.XPath("/html/body/div[1]/header/div/div[2]/div[2]/div/div/div/form/label/input[1]")).SendKeys("Selenium"+Keys.Enter);
            //Open first repo in new tab
            driver.FindElement(By.XPath("//*[@id='js-pjax-container']/div/div[3]/div/ul/li[1]/div[1]/h3/a")).SendKeys(Keys.Control + Keys.Return);
            //Get original tab handle
            string windowHandle = driver.CurrentWindowHandle;
            //Switch handle to new tab
            foreach (var window in driver.WindowHandles)
            {
                if (window != windowHandle)
                {
                    driver.SwitchTo().Window(window);
                }
            }
            //Refresh tab
            driver.Navigate().Refresh();
            Thread.Sleep(5000);
            //Check if repo name is "selenium"
            Assert.AreEqual(driver.FindElement(By.XPath("//*[@id='js-repo-pjax-container']/div[1]/div/h1/strong/a")).Text, "selenium");
            //Close tab
            driver.Close();
            //Switch handle to first tab
            driver.SwitchTo().Window(windowHandle);
            //Open link
            driver.Navigate().GoToUrl("https://selenium.dev/selenium/docs/api/dotnet/");
            //Get page source
            string pageSource = driver.PageSource;
            //Save page source to file
            SaveStringToHTMLFile(pageSource, "pageSource");
            //Check if page source have "IHasInputDevices Interface"
            Assert.IsTrue(pageSource.Contains("IHasInputDevices Interface"));
        }

        public void SaveStringToHTMLFile(string stringToSave, string fileName)
        {
            //Create file stream
            StreamWriter SW = new StreamWriter(new FileStream(fileName+".html", FileMode.Create, FileAccess.Write));
            //Write string to file
            SW.Write(stringToSave);
            //Close the stream
            SW.Close();
        }
    }
}