# WebDriverBasicFunctionsUsing
### Frame switching:
- [HTMLSwitchFramesTest Method](https://gitlab.com/s.kuzminych/webdriverbasicfunctionsusing/blob/master/SeleniumWebDriverTrialTests/SeleniumWebDriverTrialTests/Tests.cs)
***
### Tabs management:
- [GithubTabActionsTest Method](https://gitlab.com/s.kuzminych/webdriverbasicfunctionsusing/blob/master/SeleniumWebDriverTrialTests/SeleniumWebDriverTrialTests/Tests.cs)
